package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CVController {
    @GetMapping("/CV")
    public String greeting(@RequestParam(name = "visitor", required = false)
                                   String visitor, Model model) {
        model.addAttribute("visitor", visitor.equals("")||visitor == null||visitor.isEmpty()?"This is my CV":visitor+", I hope" +
                " you are interested in hiring me.");
        return "cv";
    }
}
