package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyWithWings implements FlyBehavior{
    // TODO Complete me!

    @Override
    public void fly() {
        System.out.println("I'm flying using my wings just like your everyday duck!");
    }
}
