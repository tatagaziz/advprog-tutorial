import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

public class ScoreGroupingTest {
    // TODO Implement me!
    // Increase code coverage in ScoreGrouping class
    // by creating unit test(s)!
    Map<String, Integer> scores;
    @Before
    public void setUp(){
        scores = new HashMap<>();

        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);
    }

    @Test
    public void testOutput(){
        String expectedRes = "{11=[Charlie, Foxtrot], 12=[Alice], 15=[Emi, Bob, Delta]}";
        assertEquals(expectedRes, ScoreGrouping.groupByScores(scores).toString());
    }
}