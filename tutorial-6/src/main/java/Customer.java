import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public List<Rental> getRentals() {
        return rentals;
    }

    public String getName() {
        return name;
    }

    public String statement() {
        double totalAmount = 0;
        int frequentRenterPoints = 0;

        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();
            double thisAmount = each.getThisAmount();



            // Show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t"
                    + String.valueOf(thisAmount) + "\n";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(getTotalCharge()) + "\n";
        result += "You earned " + String.valueOf(getTotalFrequentRenterPoints())
                + " frequent renter points";

        return result;
    }
    public String htmlStatement() {
        double totalAmount = 0;
        int frequentRenterPoints = 0;

        Iterator<Rental> iterator = rentals.iterator();
        String result = "<h1>Rentals for <em>" + getName() + "</em></h1><>\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();
            double thisAmount = each.getThisAmount();



            // Show figures for this rental
            result += each.getMovie().getTitle() + ": "
                    + String.valueOf(thisAmount) + "<br>\n";
        }

        // Add footer lines
        result += "<p>Amount owed is <em>" + String.valueOf(getTotalCharge()) + "</em><p>\n";
        result += "You earned <em>" + String.valueOf(getTotalFrequentRenterPoints())
                + "</em> frequent renter points<p>";

        return result;
    }

    public double getTotalCharge() {
        double result = 0;
        Iterator<Rental> rentals = this.rentals.iterator();
        while (rentals.hasNext()) {
            Rental each = rentals.next();
            result += each.getThisAmount();
        }
        return result;
    }

    public int getTotalFrequentRenterPoints() {
        int result = 0;
        Iterator<Rental> rentals = this.rentals.iterator();
        while (rentals.hasNext()) {
            Rental each = rentals.next();
            result = each.getFrequentRenterPoints(result);
        }
        return result;
    }
}