package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Tatag Aziz Prawiro on 09/03/2018.
 */
public class ThickCrustDoughTest {
    private ThickCrustDough dough;

    @Before
    public void setUp() throws Exception{
        dough = new ThickCrustDough();
    }

    @Test
    public void toStringTest(){
        String desc = dough.toString();

        assertEquals("ThickCrust style extra thick crust dough",desc);
    }
}
