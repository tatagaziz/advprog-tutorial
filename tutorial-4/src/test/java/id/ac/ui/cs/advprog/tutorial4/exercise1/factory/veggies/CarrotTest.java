package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CarrotTest {
    private Carrot sauce;

    @Before
    public void setUp() throws Exception{
        sauce = new Carrot();
    }

    @Test
    public void toStringTest(){
        String desc = sauce.toString();

        assertEquals("Carrot",desc);
    }
}
