package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;


/**
 * Created by Tatag Aziz Prawiro on 09/03/2018.
 */
public class DepokPizzaStoreTest {

    private DepokPizzaStore depokPizzaStore;
    private Pizza pizza;

    @Before
    public void setUp() throws Exception{
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void testCheese(){
        pizza = depokPizzaStore.createPizza("cheese");
        assertEquals(pizza.getName(),"Depok Style Cheese Pizza");
    }
    @Test
    public void testVeggie(){
        pizza = depokPizzaStore.createPizza("veggie");
        assertEquals(pizza.getName(),"Depok Style Veggie Pizza");
    }
    @Test
    public void testClam(){
        pizza = depokPizzaStore.createPizza("clam");
        assertEquals(pizza.getName(), "Depok Style Clam Pizza");
    }
}
