package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EggplantTest {
    private Eggplant sauce;

    @Before
    public void setUp() throws Exception{
        sauce = new Eggplant();
    }

    @Test
    public void toStringTest(){
        String desc = sauce.toString();

        assertEquals("Eggplant",desc);
    }
}
