package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Tatag Aziz Prawiro on 09/03/2018.
 */
public class FreshClamsTest {
    private FreshClams clams;

    @Before
    public void setUp() throws Exception{
        clams = new FreshClams();
    }

    @Test
    public void toStringTest(){
        String desc = clams.toString();

        assertEquals("Fresh Clams from Long Island Sound",desc);
    }
}
