package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Tatag Aziz Prawiro on 09/03/2018.
 */
public class MarinaraSauceTest {
    private MarinaraSauce sauce;

    @Before
    public void setUp() throws Exception{
        sauce = new MarinaraSauce();
    }

    @Test
    public void toStringTest(){
        String desc = sauce.toString();

        assertEquals("Marinara Sauce",desc);
    }
}
