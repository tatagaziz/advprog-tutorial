package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MushroomTest {
    private Mushroom sauce;

    @Before
    public void setUp() throws Exception{
        sauce = new Mushroom();
    }

    @Test
    public void toStringTest(){
        String desc = sauce.toString();

        assertEquals("Mushrooms",desc);
    }
}
