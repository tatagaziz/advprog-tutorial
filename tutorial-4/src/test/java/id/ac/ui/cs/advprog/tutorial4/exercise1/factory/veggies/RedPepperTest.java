package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RedPepperTest {
    private RedPepper sauce;

    @Before
    public void setUp() throws Exception{
        sauce = new RedPepper();
    }

    @Test
    public void toStringTest(){
        String desc = sauce.toString();

        assertEquals("Red Pepper",desc);
    }
}
