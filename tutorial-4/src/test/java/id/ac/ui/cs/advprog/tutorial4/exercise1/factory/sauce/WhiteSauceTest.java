package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Tatag Aziz Prawiro on 09/03/2018.
 */
public class WhiteSauceTest {
    private WhiteSauce sauce;

    @Before
    public void setUp() throws Exception{
        sauce = new WhiteSauce();
    }

    @Test
    public void toStringTest(){
        String desc = sauce.toString();

        assertEquals("Sauce with white color",desc);
    }
}
