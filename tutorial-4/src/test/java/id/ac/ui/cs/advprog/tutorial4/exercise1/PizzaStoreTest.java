package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;
/**
 * Created by Tatag Aziz Prawiro on 09/03/2018.
 */
public class PizzaStoreTest {
    private Class<?> pizzaStoreClass;

    @Before
    public void setUp() throws Exception{
        pizzaStoreClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.PizzaStore");
    }

    @Test
    public void testPizzaStoreIsAbstractClass() throws Exception{
        int classModifiers = pizzaStoreClass.getModifiers();
        assertTrue(Modifier.isAbstract(classModifiers));
    }

//    @Test
//    public void testPizzaStoreHasOrderPizzaMethod() throws Exception{
//        Method orderPizza = pizzaStoreClass.getDeclaredMethod("orderPizza", String.class);
//        int methodModifiers = orderPizza.getModifiers();
//
//        assertTrue(Modifier.isPublic(methodModifiers));
//
//        assertEquals("Pizza",orderPizza.getGenericReturnType().getTypeName());
//    }
}
