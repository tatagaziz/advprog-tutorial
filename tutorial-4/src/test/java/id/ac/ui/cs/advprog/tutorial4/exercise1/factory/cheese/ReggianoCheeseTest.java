package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Tatag Aziz Prawiro on 09/03/2018.
 */
public class ReggianoCheeseTest {
    private ReggianoCheese cheese;

    @Before
    public void setUp() throws Exception{
        cheese = new ReggianoCheese();
    }

    @Test
    public void toStringTest(){
        String desc = cheese.toString();

        assertEquals("Reggiano Cheese",desc);
    }
}
