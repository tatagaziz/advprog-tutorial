package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Tatag Aziz Prawiro on 09/03/2018.
 */
public class NewYorkPizzaStoreTest {
    private NewYorkPizzaStore newYorkPizzaStore;
    private Pizza pizza;

    @Before
    public void setUp(){
        newYorkPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void testCheese(){
        pizza = newYorkPizzaStore.createPizza("cheese");
        assertEquals(pizza.getName(),"New York Style Cheese Pizza");
    }
    @Test
    public void testVeggie(){
        pizza = newYorkPizzaStore.createPizza("veggie");
        assertEquals(pizza.getName(),"New York Style Veggie Pizza");
    }
    @Test
    public void testClam(){
        pizza = newYorkPizzaStore.createPizza("clam");
        assertEquals(pizza.getName(), "New York Style Clam Pizza");
    }
}
