package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ManchegoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.KampungClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.MargonDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.WhiteSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;

/**
 * Created by Tatag Aziz Prawiro on 09/03/2018.
 */
public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {
    @Override
    public Dough createDough() {
        return new MargonDough();
    }

    @Override
    public Sauce createSauce() {
        return new WhiteSauce();
    }

    @Override
    public Cheese createCheese() {
        return new ManchegoCheese();
    }

    @Override
    public Veggies[] createVeggies() {
        Veggies[] res = {new Carrot(), new BlackOlives(), new Garlic(), new Mushroom(), new Onion()};
        return res;
    }

    @Override
    public Clams createClam() {
        return new KampungClams();
    }
}
