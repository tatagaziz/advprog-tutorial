package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    // TODO Implement me!
    // What's missing in this Singleton declaration?
    private static Singleton instance;

    private Singleton(){};


    public static Singleton getInstance() {
        // TODO Implement me!
        if(instance == null){
            instance = new Singleton();
        }
        return instance;
    }
}
