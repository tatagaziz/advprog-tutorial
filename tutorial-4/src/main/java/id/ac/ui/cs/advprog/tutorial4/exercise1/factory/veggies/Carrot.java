package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

/**
 * Created by Tatag Aziz Prawiro on 09/03/2018.
 */
public class Carrot implements Veggies{
    public String toString(){
        return "Carrot";
    }
}
