package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

/**
 * Created by Tatag Aziz Prawiro on 09/03/2018.
 */
public class MargonDough implements Dough{
    public String toString(){
        return "This was made in Margondough";
    }
}
