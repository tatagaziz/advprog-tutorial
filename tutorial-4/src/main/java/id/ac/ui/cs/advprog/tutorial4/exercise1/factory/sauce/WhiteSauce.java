package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

/**
 * Created by Tatag Aziz Prawiro on 09/03/2018.
 */
public class WhiteSauce implements Sauce {
    public String toString() {
        return "Sauce with white color";
    }
}
