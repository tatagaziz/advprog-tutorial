package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;


/**
 * Created by Tatag Aziz Prawiro on 09/03/2018.
 */
public class ManchegoCheese implements Cheese {
    public String toString(){
        return  "Shredded Manchego";
    }
}
