package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

/**
 * Created by Tatag Aziz Prawiro on 09/03/2018.
 */
public class KampungClams implements Clams {
    public String toString(){
        return "Kampung Clam, kampung taste";
    }
}
