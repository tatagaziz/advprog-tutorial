package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Created by Tatag Aziz Prawiro on 02/03/2018.
 */
public class ChickenMeat extends Filling {
    Food food;

    @Override
    public String getDescription() {
        return food.getDescription() +", adding chicken meat";
    }

    @Override
    public double cost() {
        return food.cost() + 4.5;
    }

    public ChickenMeat(Food food) {
        this.food = food;
    }
}
