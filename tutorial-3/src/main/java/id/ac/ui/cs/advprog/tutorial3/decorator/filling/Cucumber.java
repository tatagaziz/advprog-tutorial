package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

/**
 * Created by Tatag Aziz Prawiro on 02/03/2018.
 */
public class Cucumber extends Filling{
    Food food;

    public Cucumber(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        return food.getDescription() +", adding cucumber";
    }

    @Override
    public double cost() {
        return food.cost() + 0.4;
    }
}
