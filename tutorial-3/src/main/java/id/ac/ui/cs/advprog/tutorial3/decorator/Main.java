package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChickenMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChiliSauce;

/**
 * Created by Tatag Aziz Prawiro on 02/03/2018.
 */
public class Main {
    public static void main(String[] args) {
        Food burger = new ThickBunBurger();
        burger = new BeefMeat(burger);
        burger.getDescription();
        burger = new BeefMeat(
                    new ChickenMeat(
                            new ChiliSauce(
                                    new ThickBunBurger()
                            )
                    )
        );
        burger.getDescription();
    }
}
