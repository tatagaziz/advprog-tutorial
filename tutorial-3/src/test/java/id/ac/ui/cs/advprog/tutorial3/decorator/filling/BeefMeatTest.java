package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.NoCrustSandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThinBunBurger;

import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChickenMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChiliSauce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cucumber;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Filling;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Lettuce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Tomato;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.TomatoSauce;
import org.junit.Before;
import org.junit.Test;

public class BeefMeatTest {

    private BeefMeat crustyBeefMeatSanwich;

    @Before
    public void setUp() {
        crustyBeefMeatSanwich = new BeefMeat(new CrustySandwich());
    }

    @Test
    public void testMethodCost() {
        assertEquals(7.00, crustyBeefMeatSanwich.cost(), 0.00);
    }

    @Test
    public void testMethodGetDescription() {
        assertEquals("Crusty Sandwich, adding beef meat",
                crustyBeefMeatSanwich.getDescription());
    }
}
